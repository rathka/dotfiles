# /etc/bashrc
#
# https://wiki.archlinux.org/index.php/Color_Bash_Prompt
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output. So make sure this doesn't display
# anything or bad things will happen !

# Test for an interactive shell. There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.

# If not running interactively, don't do anything!
[[ $- != *i* ]] && return

HISTCONTROL=ignoredups
HISTCONTROL=ignoreboth

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

# Enable history appending instead of overwriting.
shopt -s histappend

case ${TERM} in
  xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
    ;;
  screen)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033_%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/~}"'
    ;;
esac

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS. Try to use the external file
# first to take advantage of user additions. Use internal bash
# globbing instead of external grep binary.

# sanitize TERM:
safe_term=${TERM//[^[:alnum:]]/?}
match_lhs=""

[[ -f ~/.dir_colors ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs} ]] \
  && type -P dircolors >/dev/null \
  && match_lhs=$(dircolors --print-database)

# add colors to alacritty
match_lhs="TERM alacritty TERM foot ${match_lhs}"

if (for e in $(sed 's/TERM /TERM_/g' <<<$match_lhs); do [[ TERM_$safe_term == $e ]] && break; done) ; then
  
  # we have colors :-)

  # Enable colors for ls, etc. Prefer ~/.dir_colors
  if type -P dircolors >/dev/null ; then
    if [[ -f ~/.dir_colors ]] ; then
      eval $(dircolors -b ~/.dir_colors)
    elif [[ -f /etc/DIR_COLORS ]] ; then
      eval $(dircolors -b /etc/DIR_COLORS)
    fi
  fi

  PS1="$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]\h'; else echo '\[\033[01;32m\]\u@\h'; fi)\[\033[01;34m\] \W \$(if [[ \$? != 0 ]]; then echo '\[\033[01;31m\]!'; else echo '\$'; fi)\[\033[00m\] "

  alias ls="ls --color=auto"
  alias dir="dir --color=auto"
  alias grep="grep --color=auto"
  alias dmesg='dmesg --color'

else
  # show root@ when we do not have colors
  PS1="\u@\h \W \$(if [[ \$? != 0 ]]; then echo '!'; else echo '\$'; fi) "
fi

PS2="> "
PS3="> "
PS4="+ "

# Try to keep environment pollution down, EPA loves us.
unset safe_term match_lhs

# Try restrictive 'which' args
_which_args='--skip-alias --skip-functions --skip-dot --skip-tilde'
which ${_which_args} sh &>/dev/null || _which_args=''

_which(){
  for cmd in $@; do
    which ${_which_args} ${cmd} && return
  done
}

_bashrc_aliases(){
  local run0=$(_which run0 sudo)
  alias l='ls'
  alias la='ls -A'
  alias ll='ls -lh'
  alias rm='rm -I'
  _which htop &&
    alias top='htop'
  _which vim &&
    alias vi='vim' &&
    alias svi="${run0} vim"
  _which sudo ||
    alias sudo="${run0}"
  _which powertop &&
    alias powertop="${run0} powertop" &&
    alias powertune='powertop --auto-tune'
  alias dmesg="${run0} dmesg"
  eval "$(grep alias ${HOME}/.ssh/agent.sh)"
}

_bashrc_bash_completion(){
  # Requires 'bash-completion' package.
  shot -oq posix && return
  source ~/.local/share/bash-completion/bash_completion ||
    source /usr/share/bash-completion/bash_completion
}

_bashrc_command_not_found_hook(){
  # Requires 'pkgfile' package.
  source /usr/share/doc/pkgfile/command-not-found.bash
}

_bashrc_gnome_terminal(){
  [ -n $TILIX_ID ] || [ -n $VTE_VERSION ] || return
  source /etc/profile.d/vte.sh
}

_bashrc_local_sources(){
  local env_file
  for env_file in ${HOME}/.env/*.sh; do source ${env_file}; done
}

_clean_path(){
  local clean_path
  for path in ${PATH}; do
    echo ${clean_path} | grep -q ${path} || clean_path="${clean_path}:${path}"
  done
  echo "${clean_path#:*}"
}

for func in $(declare -F | sed -n 's|declare -f \(_bashrc_\)|\1|p'); do
  eval "${func} &>/dev/null; unset ${func}"
done
export PATH=$(IFS=: _clean_path)
unset _clean_path _which _which_args

# Finish gracefully
ls "${HOME}/downloads/" 2>/dev/null
true
