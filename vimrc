set autoindent
set autoread
set expandtab
set fdm=marker fdl=0
set linebreak
set textwidth=120
set nocompatible
set smartindent
set shiftwidth=2
set softtabstop=2
set tabstop=8
set hlsearch
set incsearch

" set number

filetype plugin indent on
syntax on

map <silent> <F2> :NERDTreeToggle<CR>
map <silent> <F3> :set number! relativenumber!<CR>
map <silent> <F4> :setlocal spell! spelllang=en_us<CR>

autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0
