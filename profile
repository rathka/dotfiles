export PATH="${HOME}/.local/bin:${PATH}"
[ -r "${HOME}/.ssh/agent.sh" ] && . "${HOME}/.ssh/agent.sh"
[ -d "${XDG_RUNTIME_DIR}" ] && export TMUX_TMPDIR=${XDG_RUNTIME_DIR}
