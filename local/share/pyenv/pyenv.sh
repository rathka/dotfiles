#!/bin/env dash

cmd=$(basename $0)
CMD=$(which -a ${cmd} | grep -v "${HOME}" | head -n1)
exclude_shims='pip|easy'


symlink_shims(){
  local pyenv_bin=${PYENV_ROOT}/bin
  local pyenv_exc=${PYENV_ROOT}/pyenv.sh
  mkdir ${pyenv_bin} 2>/dev/null || rm ${pyenv_bin}/* 2>/dev/null
  for shim in $(ls ${PYENV_ROOT}/shims | grep -Ev ${exclude_shims}); do
    ln -s ${pyenv_exc} ${pyenv_bin}/${shim}
  done
}


pyenv_wrapper(){
  case $1 in
    init)
      if [ $# -eq 1 ]; then
        pyenv_root=$(realpath -s --relative-base="${HOME}" "${PYENV_ROOT}")
        echo '# Enable pyenv by appending this output to your shell profile' >&2
        ${CMD} $@ 2>&1 | grep '\$' | sed "s|\(HOME/\).*|\1${pyenv_root}\"|"
        echo "alias pyvenv=\". \${HOME}/${pyenv_root}/pyvenv.sh\""
      else
        ${CMD} $@ | sed "s|\(${PYENV_ROOT}\)/shims|\1/bin|g"
      fi;;
    global)
      echo 'info: global command deactivated.'; exit 0;;
    install|uninstall)
      ${CMD} $@ && symlink_shims;;
    *)
      ${CMD} $@;;
  esac
}


export PYENV_ROOT=$(dirname "$(realpath $0)")
case ${cmd} in
  pyenv.sh)
    mkdir -p ${HOME}/.local/bin ${PYENV_ROOT}/shims
    ln -fs $(realpath $0) ${HOME}/.local/bin/pyenv 
    symlink_shims
    ${HOME}/.local/bin/pyenv init;;
  pyenv)
    CMD=${CMD:-$(find "${PYENV_ROOT}" -type f -executable -name "${cmd}")}
    if [ -z "${CMD}" ]; then
      echo 'err: is seems pyenv is not installed!'
      exit 1
    fi
    pyenv_wrapper $@;;
  *)
    if pyenv local > /dev/null 2>&1; then
      exec "${PYENV_ROOT}/shims/${cmd}" $@
    elif [ -n "${CMD}" ]; then
      exec "${CMD}" $@
    else
      echo 'err: local pyenv not set!'
      exit 1
    fi;;
esac
