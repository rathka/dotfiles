python -V >/dev/null 2>&1 || return $?
env_file='env'
venv=$(realpath ${1:-${VIRTUAL_ENV:-$(mktemp -d /tmp/venv_$(python -V | cut -d\  -f2).XXX)}})
mkdir -p ${venv}

dst_env=${venv}/${env_file}
tmp_env=$(mktemp)
checksum=sha1sum

cat > ${tmp_env} << EOF
export VENV_HOME=${venv}
export VENV_FILE=\${VENV_HOME}/${env_file}
export PRJ_TEMP=\${VENV_HOME}/tmp

_check_venv(){
  [ -z "\${VIRTUAL_ENV}" ] && echo "\${VENV_HOME} deactivated!" && return 1
  local tmp_env=\$(mktemp)
  tail -n +2 \${VENV_FILE} > \${tmp_env} 2>/dev/null &&
  echo "\${VENV_CHECKSUM}  \${tmp_env}" | ${checksum} -c --status 2>/dev/null
  status=\$?
  rm \${tmp_env}
  [ \${status} -ne 0 ] && echo 'destroying broken environment!' && venv_destroy
  return \${status}
}

venv_reset(){
  VIRTUAL_ENV=\${VENV_HOME}
  _check_venv || return
  mv \${VENV_FILE} \${VENV_HOME}/.${env_file}
  rm -rf \${VENV_HOME}/*
  mv \${VENV_HOME}/.${env_file} \${VENV_FILE}
  python3 -m venv \${VENV_HOME}
  . \${VIRTUAL_ENV}/bin/activate
  pip install --upgrade pip
  pip install wheel
}

venv_path(){
  _check_venv || return
  echo \${VENV_FILE}
}

venv_reactivate(){
  VIRTUAL_ENV=\${VENV_HOME}
  _check_venv || return
  . \${VIRTUAL_ENV}/bin/activate 2>/dev/null || venv_reset
}

venv_deactivate(){
  [ -n "\${VIRTUAL_ENV}" ] && deactivate
}

venv_destroy(){
  venv_deactivate
  rm -rf \${VENV_HOME} 2>/dev/null
  unset VENV_CHECKSUM VENV_FILE VENV_HOME
  unset venv_reactivate venv_deactivate venv_destroy venv_path venv_reset venv_symlink
  unset _check_venv
}

venv_symlink(){
  ln -s "\${VENV_HOME}" \$@
}

venv_deactivate 2>/dev/null
venv_reactivate
EOF

mkdir -p ${venv}
[ -f "${dst_env}" ] ||
  ( echo "export VENV_CHECKSUM=$(${checksum} ${tmp_env} | cut -d\  -f1)"; cat ${tmp_env} ) > ${dst_env}

rm ${tmp_env}
. ${dst_env}
unset env_file venv dst_env tmp_env checksum
