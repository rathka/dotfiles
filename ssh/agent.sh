SSH_AGENT=/usr/bin/ssh-agent
SSH_AGENT_ENV=${HOME}/.ssh/agent.env
GCR_SOCKET=${XDG_RUNTIME_DIR}/gcr/ssh

agent_missing(){
  /usr/bin/ssh-add -l >/dev/null 2>&1
  [ $? -gt 1 ]
}

if [ -d "${XDG_RUNTIME_DIR}" ]; then
  # configure ssh-agent to use ${XDG_RUNTIME_DIR}
  SSH_AGENT_ENV="${XDG_RUNTIME_DIR}/ssh-agent/env"
  SSH_AGENT="${SSH_AGENT} -a ${XDG_RUNTIME_DIR}/ssh-agent/socket"
fi

# try possibly running agents
[ -S "${GCR_SOCKET}" ] && agent_missing && export SSH_AUTH_SOCK="${GCR_SOCKET}"

if agent_missing; then
  # start ssh-agent
  . "${SSH_AGENT_ENV}" || mkdir -pm 700 "$(dirname "${SSH_AGENT_ENV}")"
  agent_missing && eval "${SSH_AGENT} > ${SSH_AGENT_ENV} && . ${SSH_AGENT_ENV}"
fi >/dev/null 2>&1

unset GCR_SOCKET SSH_AGENT SSH_AGENT_ENV agent_missing
alias ssh-id='(cd ${HOME}/.ssh && ssh-add -t ${SSH_AGENT_TTL:-43200} $(grep -rl "PRIVATE KEY" id))'
